-- phpMyAdmin SQL Dump
-- version 5.1.0
-- https://www.phpmyadmin.net/
--
-- Хост: 127.0.0.1
-- Время создания: Дек 15 2021 г., 22:01
-- Версия сервера: 10.4.18-MariaDB
-- Версия PHP: 8.0.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `touragency`
--

-- --------------------------------------------------------

--
-- Структура таблицы `hotel`
--

CREATE TABLE `hotel` (
  `idHotel` int(11) NOT NULL,
  `Name` varchar(45) NOT NULL,
  `Rating` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `hotel`
--

INSERT INTO `hotel` (`idHotel`, `Name`, `Rating`) VALUES
(1, 'Zeus Hotel', 10),
(2, 'Not that good hotel', 3),
(16, 'x', 0),
(17, '56', 0),
(18, '56', 0),
(19, 'A', 0);

-- --------------------------------------------------------

--
-- Структура таблицы `profile`
--

CREATE TABLE `profile` (
  `idProfile` int(11) NOT NULL,
  `Email` varchar(45) NOT NULL,
  `Password` varchar(45) NOT NULL,
  `Name` varchar(45) NOT NULL,
  `Surname` varchar(45) NOT NULL,
  `DateOfBirth` varchar(45) NOT NULL,
  `isManager` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `profile`
--

INSERT INTO `profile` (`idProfile`, `Email`, `Password`, `Name`, `Surname`, `DateOfBirth`, `isManager`) VALUES
(1, '1', '1', '1', '1', '21-01-2002', 0),
(5, 'Manager', 'Manager', 'Ivan', 'Sergejev', '14.01.2002', 1);

-- --------------------------------------------------------

--
-- Структура таблицы `profile_has_voucher`
--

CREATE TABLE `profile_has_voucher` (
  `Profile_idProfile` int(11) NOT NULL,
  `Voucher_idVoucher` int(11) NOT NULL,
  `id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `voucher`
--

CREATE TABLE `voucher` (
  `idVoucher` int(11) NOT NULL,
  `Name` varchar(45) NOT NULL,
  `Price` varchar(45) NOT NULL,
  `Country` varchar(45) NOT NULL,
  `Additional_services` varchar(150) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `voucher`
--

INSERT INTO `voucher` (`idVoucher`, `Name`, `Price`, `Country`, `Additional_services`) VALUES
(1, 'Cyprus tour', '260', 'Cyprus', ''),
(2, 'Mediteranian tour', '370', 'Greece', ''),
(23, 'A', 'A', 'A', '');

-- --------------------------------------------------------

--
-- Структура таблицы `voucher_has_hotel`
--

CREATE TABLE `voucher_has_hotel` (
  `Voucher_idVoucher` int(11) NOT NULL,
  `Hotel_idHotel` int(11) NOT NULL,
  `id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `voucher_has_hotel`
--

INSERT INTO `voucher_has_hotel` (`Voucher_idVoucher`, `Hotel_idHotel`, `id`) VALUES
(1, 1, 1),
(2, 2, 2),
(21, 17, 5),
(22, 18, 6),
(23, 19, 7);

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `hotel`
--
ALTER TABLE `hotel`
  ADD PRIMARY KEY (`idHotel`);

--
-- Индексы таблицы `profile`
--
ALTER TABLE `profile`
  ADD PRIMARY KEY (`idProfile`);

--
-- Индексы таблицы `profile_has_voucher`
--
ALTER TABLE `profile_has_voucher`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_Profile_has_Voucher_Voucher1_idx` (`Voucher_idVoucher`),
  ADD KEY `fk_Profile_has_Voucher_Profile1_idx` (`Profile_idProfile`);

--
-- Индексы таблицы `voucher`
--
ALTER TABLE `voucher`
  ADD PRIMARY KEY (`idVoucher`);

--
-- Индексы таблицы `voucher_has_hotel`
--
ALTER TABLE `voucher_has_hotel`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_Voucher_has_Hotel_Hotel1_idx` (`Hotel_idHotel`),
  ADD KEY `fk_Voucher_has_Hotel_Voucher_idx` (`Voucher_idVoucher`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `hotel`
--
ALTER TABLE `hotel`
  MODIFY `idHotel` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT для таблицы `profile`
--
ALTER TABLE `profile`
  MODIFY `idProfile` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT для таблицы `profile_has_voucher`
--
ALTER TABLE `profile_has_voucher`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=55;

--
-- AUTO_INCREMENT для таблицы `voucher`
--
ALTER TABLE `voucher`
  MODIFY `idVoucher` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT для таблицы `voucher_has_hotel`
--
ALTER TABLE `voucher_has_hotel`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- Ограничения внешнего ключа сохраненных таблиц
--

--
-- Ограничения внешнего ключа таблицы `profile_has_voucher`
--
ALTER TABLE `profile_has_voucher`
  ADD CONSTRAINT `fk_Profile_has_Voucher_Profile1` FOREIGN KEY (`Profile_idProfile`) REFERENCES `profile` (`idProfile`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_Profile_has_Voucher_Voucher1` FOREIGN KEY (`Voucher_idVoucher`) REFERENCES `voucher` (`idVoucher`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Ограничения внешнего ключа таблицы `voucher_has_hotel`
--
ALTER TABLE `voucher_has_hotel`
  ADD CONSTRAINT `fk_Voucher_has_Hotel_Hotel1` FOREIGN KEY (`Hotel_idHotel`) REFERENCES `hotel` (`idHotel`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_Voucher_has_Hotel_Voucher` FOREIGN KEY (`Voucher_idVoucher`) REFERENCES `voucher` (`idVoucher`) ON DELETE NO ACTION ON UPDATE NO ACTION;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
